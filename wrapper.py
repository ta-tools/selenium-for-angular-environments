from SeleniumLibrary.errors import ElementNotFound
from selenium.common.exceptions import (
    StaleElementReferenceException, 
    ElementNotVisibleException, 
    ElementClickInterceptedException,
    ElementNotInteractableException
)
import time

def wait_js(function):
    """
    Wrapper to handle issues with Angular JS loading
    """
    def wrapper(self, *args, **kwargs):
        timeout = time.time() + self._timeout
        while (time.time() < timeout):          
            try:
                return_value = function(self, *args, **kwargs)
                return return_value
            except (
                StaleElementReferenceException, 
                ElementClickInterceptedException, 
                ElementNotFound, 
                ElementNotVisibleException,
                ElementNotInteractableException):
                time.sleep(0.2)
        try:
            return_value = function(self, *args, **kwargs)
            return return_value
        except (BaseException) as error:
            raise error
    return wrapper


def wait_js_kw(function):
    """
    Wrapper to handle issues with Angular JS loading
    """
    def wrapper(*args, timeout=10, **kwargs):
        timeout = time.time() + timeout
        while (time.time() < timeout):          
            try:
                return_value = function(*args, **kwargs)
                return return_value
            except (
                StaleElementReferenceException, 
                ElementClickInterceptedException, 
                ElementNotFound, 
                ElementNotVisibleException,
                ElementNotInteractableException):
                time.sleep(0.2)
        try:
            return_value = function(*args, **kwargs)
            return return_value
        except (BaseException) as error:
            raise error
    return wrapper
