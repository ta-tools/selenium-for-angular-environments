# Selenium For Angular Environments

## Example of overriding SeleniumLibrary to use in angular environments where a lot of StaleElementExceptions are bound to happen

Contains example code for overriding selenium keywords to use in angular environments if encountering a lot of stale element exceptions and such.
Same concept can be used in other ways. Idea is to use a wrapper that tries to complete any kw ignoring selenium issues until it succeeds.
Wrapper can, surprisingly, be found in wrapper.py

## License
Use however.
