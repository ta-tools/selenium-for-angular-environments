*** Settings ***
Library         SeleniumLibrary
Library         examples/run_keyword_example.py


*** Test Cases ***
Test click element
    [Teardown]    Close All Browsers
    Open Browser    https://www.example.com
    Run Keyword With Wait    Click Element    //*[contains(@href, 'domains/example')]
    Title Should Be    Example Domains
