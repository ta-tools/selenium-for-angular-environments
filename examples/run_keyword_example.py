from wrapper import wait_js_kw
from robot.libraries.BuiltIn import BuiltIn


@wait_js_kw
def run_keyword_with_wait(name, *args):
    return BuiltIn().run_keyword(name, *args)
