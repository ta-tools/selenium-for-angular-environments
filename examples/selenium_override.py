from SeleniumLibrary import SeleniumLibrary
from wrapper import wait_js


class selenium_override(SeleniumLibrary):
    """
    Simple set of keywords using selenium & SeleniumLibrary with angular support
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._timeout = 10

    @wait_js
    def click_element(self, *args, **kwargs):
        """
        Clicks the element @locator
        """
        super().click_element(*args, **kwargs)

    @wait_js
    def find_element(self, *args, **kwargs):
        """
        Returns element if found, else None
        """
        return super().find_element(*args, **kwargs)
